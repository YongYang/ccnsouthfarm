This CCNx application retrieves images from the camera attached to each node. 

ccnCamCapture runs on the sensor node to handle CCN interests images from the camera.

On the basestation, ccnCamDisplay periodically sends out a CCNx interest in  the current image from a particular node. When it receives a data package (an image), it displays on the GUI.

To compile, you need to include the ccn.jar in the CLASS_PATH:
javac -cp .:ccn.jar ccnCamCapture.java ccnCamDisplay.java

To run,
java -cp .:ccn.jar:bcprov-jdk15-143.jar ccnCamCapture -node NODEID -dev /dev/video0
java -cp .:ccn.jar:bcprov-jdk15-143.jar ccnCamDisplay

