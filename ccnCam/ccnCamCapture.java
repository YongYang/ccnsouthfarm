import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.awt.Font;


import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.CCNFileOutputStream;
import org.ccnx.ccn.io.CCNVersionedOutputStream;
import org.ccnx.ccn.profiles.CommandMarker;
import org.ccnx.ccn.profiles.SegmentationProfile;
import org.ccnx.ccn.profiles.VersioningProfile;
import org.ccnx.ccn.profiles.metadata.MetadataProfile;
import org.ccnx.ccn.profiles.security.KeyProfile;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;
import org.ccnx.ccn.utils.CommonSecurity;


public class ccnCamCapture  implements CCNFilterListener{
	

    protected ContentName _prefix; 
    protected CCNHandle _handle;


    public final static String defual_namespace="ccnx:/uiuc.edu/apps/southfarm/";
    private final static String application = "/cam";
    public static String nodeid = "base";

    
    public static int BUF_SIZE = 4096;
    
    public static String file_buf_name = "./00000001.jpg";
    public static String device = "/dev/video0";

    
    public static int width =   640;
    public static int height = 480;
    public static SimpleDateFormat dateformat = new SimpleDateFormat ("yyyy.MM.dd HH:mm:ss.SSS z");

    public ccnCamCapture()throws MalformedContentNameStringException, ConfigurationException, IOException {
    	
        _prefix = ContentName.fromURI(defual_namespace+nodeid+application);
        _handle = CCNHandle.open();
    }


    public boolean handleInterest(Interest interest) {
        // Alright, we've gotten an interest. Either it's an interest for a stream we're
        // already reading, or it's a request for a new stream.

        // Test to see if we need to respond to it.
        if (!_prefix.isPrefixOf(interest.name())) {
                Log.info("Unexpected: got an interest not matching our prefix (which is {0})", _prefix);
                return false;
        }

        // We see interests for all our segments, and the header. We want to only
        // handle interests for the first segment of a file, and not the first segment
        // of the header. Order tests so most common one (segments other than first, non-header)
        // fails first.
        if (SegmentationProfile.isSegment(interest.name()) && !SegmentationProfile.isFirstSegment(interest.name())) {
                Log.info("Got an interest for something other than a first segment, ignoring {0}.", interest.name());
                return false;
        } else if (interest.name().contains(CommandMarker.COMMAND_MARKER_BASIC_ENUMERATION.getBytes())) {
                        return false;

        } else if (MetadataProfile.isHeader(interest.name())) {
                Log.info("Got an interest for the first segment of the header, ignoring {0}.", interest.name());
                return false;
        } 
        try {
                return capture(interest);
        } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
        }
    }
    
	//Capture one image frame from the camera device, and buffer it in a temporary file
    protected InputStream getImageStream(){
		String cmd = "mplayer tv:// -tv driver=v4l2:device="+device+":width=320:height=240:norm=1 -frames 1 -vo jpeg";
		try {
			System.out.println("executing.............");
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		try {
			FileInputStream fis = new FileInputStream(file_buf_name);
		   	return fis;
	    }catch(FileNotFoundException e){
    		e.printStackTrace();
    		return null;
    	}
    }
	//Capture one image frame from the camera device, and buffer it in a temporary file, and add a timestamp on it
    protected InputStream getImageStream_version(){
		String cmd = "mplayer tv:// -tv driver=v4l2:device="+device+":width=" +width+ ":height=" +height+ ":norm=1 -frames 1 -vo jpeg";
		try {
				System.out.println("executing.............");
				Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
		}

		try {
				FileInputStream fis = new FileInputStream(file_buf_name);
				BufferedImage img = ImageIO.read(fis);
				Graphics2D graph  = img.createGraphics(); 
				graph.setFont(new Font("Default", Font.PLAIN, 9));
				Calendar now = Calendar.getInstance();
				graph.drawString(dateformat.format(now.getTime()), width - 145, height - 20 );
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(img, "jpg", os);
				InputStream is = new ByteArrayInputStream(os.toByteArray());

				return is;
		}catch(Exception e){
				e.printStackTrace();    
				return null;
		}
    }



    protected long lastModified(){
		File file_buf = new File (file_buf_name);
		return file_buf.lastModified();
    }
    
    
    protected synchronized boolean capture(Interest outstandingInterest) throws IOException{
    	
	    ContentName temp = outstandingInterest.name();
		
		if (VersioningProfile.isVersionComponent(temp.lastComponent() ) || SegmentationProfile.isSegmentMarker(temp.lastComponent()) )
			temp = temp.cut(temp.count() -1);
		if (VersioningProfile.isVersionComponent(temp.lastComponent() ) || SegmentationProfile.isSegmentMarker(temp.lastComponent()) )
			temp = temp.cut(temp.count() -1);
		
		System.out.println("=============got an interest "+outstandingInterest+"===================");
		//InputStream fis = getImageStream();
		InputStream fis = getImageStream_version();
		if (fis == null){
			Log.warning("Unexpected: can not open image stream");
			return false;
		}
    
		// Set the version of the CCN content to be the last modification time of the file.
		CCNTime modificationTime = new CCNTime();

		ContentName versionedName =	VersioningProfile.addVersion(new ContentName(_prefix,
				temp.postfix(_prefix).components()), modificationTime);

		// CCNFileOutputStream will use the version on a name you hand it (or if the name
		// is unversioned, it will version it).
		CCNVersionedOutputStream ccnout = new CCNVersionedOutputStream(versionedName, _handle);
		ccnout.setFreshnessSeconds(new Integer(20));
		
		// We have an interest already, register it so we can write immediately.
		ccnout.addOutstandingInterest(outstandingInterest);
		
		byte [] buffer = new byte[BUF_SIZE];
		
		int read = fis.read(buffer);
		while (read >= 0) {
			ccnout.write(buffer, 0, read);
			read = fis.read(buffer);
		} 
		fis.close();
		ccnout.close(); // will flush
		
		return true;
		
    }


	public void start() throws IOException{
		// All we have to do is say that we're listening on our main prefix.
		_handle.registerFilter(_prefix, this);
		System.out.println("Starting picture capture service of device " + device + " on CCNx namespace " + _prefix + "...");
		
	}    

	public static void parseArg(String[] args){
		int startArg = 0;

		for (int i = 0; i < args.length ; i++) {

			if (args[i].equals("-dev")) {
				if (args.length < (i + 2)) {
					usage();
					System.exit(1);
				}
				ccnCamCapture.device = args[++i];
				if (startArg <= i)
					startArg = i + 1;
			} else if (args[i].equals("-node")) {
				if (args.length < (i + 2)) {
					usage();
					System.exit(1);
				}
				ccnCamCapture.nodeid = args[++i];
				if (startArg <= i)
					startArg = i + 1;
			}else {
				usage();
				System.exit(1);
			}
		}
		
		
	}
	private static void usage(){
		System.out.println("usage: java ccnCamCaputure [-dev VideoDevice] [-node NodeID]");
	}
	
    public static void main(String[] args) throws Exception {
    	parseArg(args);
    	
    	ccnCamCapture cam = new ccnCamCapture();
    	cam.start();
		while (true) {
			// we really want to wait until someone ^C's us.
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e) {
				// do nothing
			}
		}
    }
    
    
}
