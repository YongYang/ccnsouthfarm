import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Calendar;


import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.CCNFileInputStream;
import org.ccnx.ccn.io.CCNVersionedInputStream;
import org.ccnx.ccn.profiles.CommandMarker;
import org.ccnx.ccn.profiles.SegmentationProfile;
import org.ccnx.ccn.profiles.VersioningProfile;
import org.ccnx.ccn.profiles.metadata.MetadataProfile;
import org.ccnx.ccn.profiles.security.KeyProfile;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;


public class ccnCamDisplay extends Panel implements Runnable,WindowListener{
	
    BufferedImage  image1;
    BufferedImage  image2;
    
    private BufferedImage  image = null;


    protected boolean finished = false;
    protected ContentName _prefix; 
    protected ContentName last_prefix; 
    protected CCNHandle _handle;


    public static String defual_namespace="ccnx:/uiuc.edu/apps/southfarm/";
	//Default node ID
    public static String nodeid = "node106";
	//Application ID
    public static String apps = "/cam/";
   
    public static int _readsize = 1024;
    
    
    public ccnCamDisplay()throws MalformedContentNameStringException, ConfigurationException, IOException {
        _prefix = ContentName.fromURI(defual_namespace+nodeid+apps);
        last_prefix = _prefix;
        _handle = CCNHandle.open();

    }

    public void paint(Graphics g) {
    	if (image != null)
    		g.drawImage( image, 0, 0, null);
    }

    long last_scheduled_time = 0 ;
	//cycle of refreshing the image from the node
    long duty_cycle = 500; //in millisec
    
    public void run() {
    	

		while(!finished){
			
			CCNVersionedInputStream input;
			
			try {
				input = new CCNVersionedInputStream(_prefix, _handle);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			input.setTimeout((int)duty_cycle/2);
		
			byte [] buffer = new byte[_readsize];
			ByteArrayOutputStream byteouts = new ByteArrayOutputStream();
			int readcount = 0;
			long readtotal = 0;
			try {
				while ((readcount = input.read(buffer)) != -1){
					readtotal += readcount;
					byteouts.write(buffer, 0, readcount);
				}
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			
			ContentName received_name = VersioningProfile.addVersion(input.getBaseName(), input.getVersion());
			ContentName received_name2 = VersioningProfile.firstBlockLatestVersionInterest(received_name, null).name();
			System.out.println(received_name);
			System.out.println(received_name2);
			ByteArrayInputStream byteinps = new ByteArrayInputStream(byteouts.toByteArray());

			
			if (image== null){
				try {
					image = ImageIO.read(byteinps);
				} catch (IOException e) {
					e.printStackTrace();
					continue;
				}				
			}else{
				synchronized (image){
					try {
						image = ImageIO.read(byteinps);
					} catch (IOException e) {
						e.printStackTrace();
						continue;
					}
				}
			}



    		//show the image
    		this.repaint();
    		
			
    		try {
    			Calendar now = Calendar.getInstance();
    			if (now.getTimeInMillis() - last_scheduled_time <=duty_cycle )
					Thread.sleep(duty_cycle - (now.getTimeInMillis() - last_scheduled_time));
    			now = Calendar.getInstance();
    			last_scheduled_time = now.getTimeInMillis(); 
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			
			}
    		
    	}
		System.exit(0);
    }

	public static void parseArg(String[] args){
		int startArg = 0;
		
		for (int i = 0; i < args.length ; i++) {

			if (args[i].equals("-node")) {
				if (args.length < (i + 2)) {
					usage();
					System.exit(1);
				}
				nodeid = args[++i];
				if (startArg <= i)
					startArg = i + 1;
			}else {
				usage();
				System.exit(1);
			}
		}
		
		
	}

	private static void usage(){
		System.out.println("usage: java ccnCamDisplay [-node NodeID]");
	}

    public static void main(String args[]) throws Exception {
    	parseArg(args);
    	
    	JFrame frame = new JFrame("Webcam of " +nodeid);
    	Panel panel = new ccnCamDisplay();
    	frame.addWindowListener((WindowListener)panel);
    	frame.getContentPane().add(panel);
    	frame.setSize(340, 260);
    	frame.setVisible(true);
    	
    	Thread th = new Thread((Runnable)panel);
    	th.start();
  }
    
    
    
    public void update(Graphics g){
    	paint(g);
    }


	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("========closed=================================");
	}

	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("========closing=================================");
		finished = true;
	}

	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
