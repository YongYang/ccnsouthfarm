This CCNx application retrieves data samples from the Phidget sensors attached to each node. 

ccnPhidget runs on the sensor node and handles the interests for Phidget sensor data.

At the basestation, ccnCamDisplay periodically sends out a CCNx interest for the Phidget sensor data of a particular node. When it receives a data package (a sensor data sample), it displays it on the Oscilloscope.

To compile, you need to include the ccn.jar and phidget21.jar (a library for Phidget APIs) in the CLASS_PATH:
javac -cp .:ccn.jar:phidget21.jar ccnPhidget.java ccnOsciloscope.java

To run,
java -cp .:ccn.jar:bcprov-jdk15-143.jar:phidget21.jar -Djava.library.path=.:/usr/local/lib  ccnPhidget -node NODE_ID

java -cp .:ccn.jar:bcprov-jdk15-143.jar ccnOsciloscope
