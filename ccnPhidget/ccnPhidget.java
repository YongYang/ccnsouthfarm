import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


import org.ccnx.ccn.CCNFilterListener;
import org.ccnx.ccn.CCNHandle;
import org.ccnx.ccn.config.ConfigurationException;
import org.ccnx.ccn.impl.support.Log;
import org.ccnx.ccn.io.CCNFileOutputStream;
import org.ccnx.ccn.io.CCNVersionedOutputStream;
import org.ccnx.ccn.profiles.CommandMarker;
import org.ccnx.ccn.profiles.SegmentationProfile;
import org.ccnx.ccn.profiles.VersioningProfile;
import org.ccnx.ccn.profiles.metadata.MetadataProfile;
import org.ccnx.ccn.protocol.CCNTime;
import org.ccnx.ccn.protocol.ContentName;
import org.ccnx.ccn.protocol.Interest;
import org.ccnx.ccn.protocol.MalformedContentNameStringException;


import com.phidgets.*;
import com.phidgets.event.*;

public class ccnPhidget  implements CCNFilterListener{

    protected ContentName _prefix; 
    protected CCNHandle _handle;

    public final static String defual_namespace="ccnx:/uiuc.edu/apps/southfarm/";
    public static String nodeid = "base";
    public static String app = "/phidget";

    public static InterfaceKitPhidget ik;
     
    public ccnPhidget()throws MalformedContentNameStringException, ConfigurationException, IOException {
    	
        _prefix = ContentName.fromURI(defual_namespace+nodeid+app);
        _handle = CCNHandle.open();
    }


    public boolean handleInterest(Interest interest) {
        // Alright, we've gotten an interest. Either it's an interest for a stream we're
        // already reading, or it's a request for a new stream.

        // Test to see if we need to respond to it.
        if (!_prefix.isPrefixOf(interest.name())) {
                Log.info("Unexpected: got an interest not matching our prefix (which is {0})", _prefix);
                return false;
        }

        // We see interests for all our segments, and the header. We want to only
        // handle interests for the first segment of a file, and not the first segment
        // of the header. Order tests so most common one (segments other than first, non-header)
        // fails first.
        if (SegmentationProfile.isSegment(interest.name()) && !SegmentationProfile.isFirstSegment(interest.name())) {
                Log.info("Got an interest for something other than a first segment, ignoring {0}.", interest.name());
                return false;
        } else if (interest.name().contains(CommandMarker.COMMAND_MARKER_BASIC_ENUMERATION.getBytes())) {
                        return false;

        } else if (MetadataProfile.isHeader(interest.name())) {
                Log.info("Got an interest for the first segment of the header, ignoring {0}.", interest.name());
                return false;
        } 
        try {
                return sense(interest);
        } catch (IOException e) {
                e.printStackTrace();
                return false;
        }
    }
    
    protected byte[] exeCommand(String command){
    	int index = 0;
    	if (command.equals("current"))		//sense charging current
    		index = 1;
    	else if (command.equals("voltage"))		//sense charging current
    		index = 0;
    	else
    		return null;
    		System.out.println("reading sensor value from port "+index);
		try {
			String value = String.valueOf(ik.getSensorValue(index));
			return value.getBytes();
		} catch (PhidgetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }

    
    protected boolean sense(Interest outstandingInterest) throws IOException{
    		System.out.println("===================== got an interest " + outstandingInterest + "===========");
    		ContentName temp = outstandingInterest.name();
		
		if (VersioningProfile.isVersionComponent(temp.lastComponent() ) || SegmentationProfile.isSegmentMarker(temp.lastComponent()) )
			temp = temp.cut(temp.count() -1);
		if (VersioningProfile.isVersionComponent(temp.lastComponent() ) || SegmentationProfile.isSegmentMarker(temp.lastComponent()) )
			temp = temp.cut(temp.count() -1);
		
		ArrayList<byte[]> cmd = temp.postfix(_prefix).components();
		if ((cmd == null ) || (cmd.size()==0))
			return false;
				
		String command = new String(cmd.get(0)).toLowerCase();
		
		byte[] result = exeCommand(command);
		
		if (result == null){
			Log.warning("Unexpected: can not get results of the command " + command);
			return false;
		}
    
		// Set the version of the CCN content to be the last modification time of the file.
	    	CCNTime modificationTime = new CCNTime();
		ContentName versionedName =	VersioningProfile.addVersion(new ContentName(_prefix,
				temp.postfix(_prefix).components()), modificationTime);

		// CCNFileOutputStream will use the version on a name you hand it (or if the name
		// is unversioned, it will version it).
		CCNVersionedOutputStream ccnout = new CCNVersionedOutputStream(versionedName, _handle);
		ccnout.setFreshnessSeconds(new Integer(1));
		
		// We have an interest already, register it so we can write immediately.
		ccnout.addOutstandingInterest(outstandingInterest);
		
		ccnout.write(result, 0, result.length);
		ccnout.close(); // will flush
		
		return true;
		
    }


	public void start() throws IOException{
		// All we have to do is say that we're listening on our main prefix.
		_handle.registerFilter(_prefix, this);
		System.out.println("Starting phiget proxy on namespace "+ _prefix + "/[current|voltage]...");		
	}    

	public static void parseArg(String[] args){
		int startArg = 0;
		
		for (int i = 0; i < args.length ; i++) {

			if (args[i].equals("-node")) {
				if (args.length < (i + 2)) {
					usage();
					System.exit(1);
				}
				nodeid = args[++i];
				if (startArg <= i)
					startArg = i + 1;
			}else {
				usage();
				System.exit(1);
			}
		}
		
		
	}
	private static void usage(){
		System.out.println("usage: java ccnPhidget [-node NodeID]");
	}
	
    public static void main(String[] args) throws Exception {
    	parseArg(args);

    	ik = new InterfaceKitPhidget();
    	ik.openAny();
	System.out.println("Waiting for interface attached");
    	ik.waitForAttachment();
    	
    	System.out.println("Phidget Interface Attached");
    	ccnPhidget server = new ccnPhidget();
    	server.start();
		while (true) {
			// we really want to wait until someone ^C's us.
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e) {
				// do nothing
			}
		}
    }
    
    
}
